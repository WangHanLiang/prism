﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Menu.ViewModels
{
    public class ViewMenuViewModel : BindableBase
    {
        public ViewMenuViewModel()
        {

        }
        private string content = "this is menu view";

        public string Content
        {
            get { return content; }
            set { SetProperty(ref content, value); }
        }
    }
}

﻿using Prism.Services.Dialogs;
using System;
using System.Windows.Controls;

namespace Content.Views
{
    /// <summary>
    /// Interaction logic for Dialog
    /// </summary>
    public partial class Dialog : UserControl
    {
        public Dialog()
        {
            InitializeComponent();
        }
    }
}

﻿using System.Windows.Controls;

namespace Content.Views
{
    /// <summary>
    /// Interaction logic for ViewContent
    /// </summary>
    public partial class ViewContent : UserControl
    {
        public ViewContent()
        {
            InitializeComponent();
        }
    }
}

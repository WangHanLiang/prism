﻿using Prism.Ioc;
using Prism.Regions;
using System.Windows;
using System.Windows.Controls;

namespace PrismApp.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {


        public MainWindow(IRegionManager regionManager)
        {
            InitializeComponent();
            // 给区域设置别名
            RegionManager.SetRegionName(HeaderRegion, "HeaderRegion");
            RegionManager.SetRegionName(MenuRegion, "MenuRegion");
            RegionManager.SetRegionName(ContentRegion, "ContentRegion");
            //建立视图与region之间的映射（View Discovery方式）       
            //regionManager.RegisterViewWithRegion("ContentRegion", typeof(Content));


        }     
    }
}

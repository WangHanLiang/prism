﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Services.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Content.ViewModels
{
    public class DialogViewModel : BindableBase, IDialogAware
    {
        public DialogViewModel()
        {

        }

        public string Title
        {
            get { return "title"; }
        }
        public event Action<IDialogResult> RequestClose;

        public bool CanCloseDialog()
        {
            return true;
        }

        //关闭回调函数
        public void OnDialogClosed()
        {

        }
        //打开回调函数
        public void OnDialogOpened(IDialogParameters parameters)
        {

        }
    }
}

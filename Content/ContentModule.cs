﻿using Content.ViewModels;
using Content.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;
using Prism.Services.Dialogs;
using Dialog = Content.Views.Dialog;

namespace Content
{
    public class ContentModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {
            //View Discovery
            var regionManager = containerProvider.Resolve<IRegionManager>();
            regionManager.RegisterViewWithRegion("ContentRegion", typeof(ViewContent));
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            //注册导航页面
            containerRegistry.RegisterForNavigation(typeof(ViewContent), "ViewContent");
            //注册dialog
            containerRegistry.RegisterDialog<Dialog>();//注册
            containerRegistry.RegisterDialog<Dialog>("DialogA");//起个别名
            containerRegistry.RegisterDialog<Dialog, DialogViewModel>();//绑定数据上下文
        }
    }
}
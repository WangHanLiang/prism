﻿using Menu.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;

namespace Menu
{
    public class MenuModule : IModule
    {
        public void OnInitialized(IContainerProvider containerProvider)
        {
            //View Discovery
            var regionManager = containerProvider.Resolve<IRegionManager>();
            regionManager.RegisterViewWithRegion("MenuRegion", typeof(ViewMenu));
        }

        public void RegisterTypes(IContainerRegistry containerRegistry)
        {
            //注册导航页面
            containerRegistry.RegisterForNavigation(typeof(ViewMenu), "ViewMenu");
        }
    }
}
﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using WpfEventAggregator;


namespace Content.ViewModels
{
    public class ViewContentViewModel : BindableBase
    {
        private string content = "this is content view";
        public string Content
        {
            get { return content; }
            set { SetProperty(ref content, value); }
        }

        #region 事件订阅
        private ObservableCollection<string> messages = new ObservableCollection<string>();
        public ObservableCollection<string> Messages
        {
            get { return messages; }
            set { SetProperty(ref messages, value); }
        }
        private IEventAggregator ea;
        public ViewContentViewModel(IEventAggregator ea)
        {
            this.ea = ea;
            ea.GetEvent<MessageSendEvent>().Subscribe(RecvMessages, ThreadOption.PublisherThread, false);
        }
        //订阅事件后的执行
        private void RecvMessages(string msg)
        {
            Messages.Add(msg);
            this.Content += msg;
        }
        #endregion
    }
}

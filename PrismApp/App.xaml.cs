﻿using Content;
using Header;
using Menu;
using Prism.Ioc;
using Prism.Modularity;
using PrismApp.Views;
using System.Windows;

namespace PrismApp
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        /// <summary>
        /// 创建主窗体
        /// </summary>
        /// <returns></returns>
        protected override Window CreateShell()
        {
            return Container.Resolve<MainWindow>();
        }
        /// <summary>
        /// 给容器注册对象
        /// </summary>
        /// <param name="containerRegistry"></param>
        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {       
            //注册为单例，程序声明周期内只创建一次       
            //containerRegistry.RegisterSingleton<Menu>();
            //containerRegistry.RegisterSingleton<Content>();
        }
        /// <summary>
        /// 重写添加模块函数
        /// </summary>
        /// <param name="moduleCatalog"></param>
        protected override void ConfigureModuleCatalog(IModuleCatalog moduleCatalog)
        {
            base.ConfigureModuleCatalog(moduleCatalog);
            moduleCatalog.AddModule<HeaderModule>();
            moduleCatalog.AddModule<MenuModule>();
            moduleCatalog.AddModule<ContentModule>();
        }
    }
}

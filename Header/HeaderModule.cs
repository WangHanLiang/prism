﻿using Header.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Regions;

namespace Header
{
    public class HeaderModule : IModule
    {
        /// <summary>
        /// 表示模块已经被初始化
        /// </summary>
        /// <param name="containerProvider"></param>
        public void OnInitialized(IContainerProvider containerProvider)
        {
            //View Discovery
            var regionManager = containerProvider.Resolve<IRegionManager>();
            regionManager.RegisterViewWithRegion("HeaderRegion", typeof(ViewA));
        }
        /// <summary>
        /// 注册对象
        /// </summary>
        /// <param name="containerRegistry"></param>
        public void RegisterTypes(IContainerRegistry containerRegistry)
        {

        }
    }
}
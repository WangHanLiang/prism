﻿using Prism.Commands;
using Prism.Events;
using Prism.Mvvm;
using Prism.Regions;
using Prism.Services.Dialogs;
using System;
using WpfEventAggregator;

namespace PrismApp.ViewModels
{
    public class MainWindowViewModel : BindableBase
    {
        private string _title = "Prism Application";
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private IEventAggregator ea;  //事件聚合器
        private IRegionManager region;//区域管理
        private IDialogService dialog;//对话框服务
        public MainWindowViewModel(IEventAggregator ea, IRegionManager regionManager, IDialogService dialogService)
        {
            this.ea = ea;
            this.region = regionManager;
            this.dialog = dialogService;
        }


        #region 事件命令
        //无参，使用cmd快捷键
        private DelegateCommand _buttonCmd;
        public DelegateCommand BtnCmd =>
            _buttonCmd ?? (_buttonCmd = new DelegateCommand(ExecuteBtnCmd));

        void ExecuteBtnCmd()
        {
            this.Title = "这是一个无参命令";
            //发布
            ea.GetEvent<MessageSendEvent>().Publish("传递过来的消息");
        }
        //有参，cmdg
        private DelegateCommand<string> _btnParams;
        public DelegateCommand<string> BtnPramCmd =>
            _btnParams ?? (_btnParams = new DelegateCommand<string>(ExecuteBtnPramCmd));

        void ExecuteBtnPramCmd(string parameter)
        {
            this.Title = "这是一个有参命令：" + parameter;
            //发布事件
            ea.GetEvent<MessageSendEvent>().Publish("有参按钮传递过来的消息");
        }

        //导航(有参)
        private DelegateCommand<string> _navigationCmd;
        public DelegateCommand<string> NavigationCmd =>
            _navigationCmd ?? (_navigationCmd = new DelegateCommand<string>(ExecuteNavigationCmd));

        void ExecuteNavigationCmd(string page)
        {
            region.RequestNavigate("ContentRegion", page);
        }
        //对话框
        private DelegateCommand _DialogCmd;
        public DelegateCommand DialogCmd =>
            _DialogCmd ?? (_DialogCmd = new DelegateCommand(ExecuteDialogCmd));

        void ExecuteDialogCmd()
        {
            this.dialog.Show("DialogA");
        }
        #endregion
    }
}
